import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuex from '../store';

import Movie from '../views/Movie.vue'
import FilmsAVoir from '../views/FilmsAVoir.vue'
import Favoris from '../views/Favoris.vue'
import Login from '../views/Login.vue'
import Home from '../views/Home.vue'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'


Vue.use(VueRouter)
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
        beforeEnter: (to, from, next) => {
          console.log(Vuex.state.session_id)
          if (Vuex.state.session_id) {
            next();
          } else {
            next({ name: 'Login' });
          }
        },
        children: [
          {
            path: 'movie/:id',
            name: 'Movie',
            component: Movie,
            props: true
          },
          {
            path: 'films-favoris',
            name: 'Favoris',
            component: Favoris,
          },
          {
            path: 'films-a-voir',
            name: 'FilmsAVoir',
            component: FilmsAVoir,
          },
        ],
      },
    {
        path:'/login',
        name:'Login',
        component : Login
    },
   
]
const router = new VueRouter({
    mode : 'history',
    routes
})
export default router;