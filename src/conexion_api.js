import axios from 'axios';

const api_key = process.env.VUE_APP_API_KEY;

const api = axios.create({
    baseURL: process.env.VUE_APP_BASE_URL
});
api.defaults.headers.common['Authorization'] = `${api_key}`;

export function createRequestToken() {
  return api.get(`authentication/token/new?api_key=${api_key}`);
}

export function logUser(username, password, token) {
  return api.post(`authentication/token/validate_with_login?api_key=${api_key}`, { username: username, password, request_token: token } );
}

export function getSessionId(token) {
  return api.post(`authentication/session/new?api_key=${api_key}`, {request_token: token });
}

export function getAccount(sessionId) {
  return api.get(`account?api_key=${api_key}&session_id=${sessionId}`);
}

export function markAsFavorite(sessionId, accountId, media ) {
    return api.post(`account/${accountId}/favorite?api_key=${api_key}&session_id=${sessionId}`, { media_id: media.media_id,  media_type: media.media_type,  favorite: media.favorite});
}

export function fetchFavoris(sessionId, accountId) {
    return api.get(`account/${accountId}/favorite/movies?api_key=${api_key}&session_id=${sessionId}`);
  }

export function getTrendingMovies() {
    return api.get(`trending/movie/day?api_key=${api_key}`);
  }