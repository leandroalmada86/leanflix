import Vue from 'vue'
import Vuex from 'vuex';
import VuexPersistence from 'vuex-persist';
import { fetchFavoris, markAsFavorite, getTrendingMovies } from '@/conexion_api';
Vue.use(Vuex)


export default new Vuex.Store({
    state:{
        account_id: '',
        session_id: '',
        requestToken: '',
        favorites: {},
        carrousel: {fetchDate: null, data: [] }
    },
    getters: {
        getInFavoriteListById: (state) => (id) => {
            if (state.favorites.data.results) {
              return state.favorites.data.results.find((favoriteItem) => favoriteItem.id === id);
            }
            return null;
          },
      },
    mutations:{
        UPDATE_VALUE: (state, payload) => {
            state[payload.name] = payload.value;
          },
          SET_FAVORIS: (state, payload) => {
            state.favorites = payload;
          },
        LOGOUT: (state, payload) => {
            console.log(payload)
            state.account_id = '';
            state.requestToken = '';
            state.session_id = '';
            state.favorites = {};
          },
          SET_CARROUSEL: (state, payload) => {
            state.carrousel = payload;
          },
    },
    actions:{
        async fetchFavoris({ commit, state }) {
            try {
              const results = await fetchFavoris(state.session_id, state.account_id);
              commit('SET_FAVORIS', results);
              console.log("BRAVO")
            } catch (e) {
                console.log(e);
            }
          },
        async addToFavorite({ commit, state, dispatch }, media) {
            try {
              await markAsFavorite(state.session_id, state.account_id, media);
              dispatch('fetchFavoris');
              console.log(commit)
            } catch (e) {
                console.log(e);
            }
          },
        async fetchCarrousel({ commit, state }) {
            const today = new Date().toISOString().split('T')[0];
      
            if (!today || !state.carrousel.data.length || today !== state.carrousel.fetchDate) {
              const results = await getTrendingMovies();
              commit('SET_CARROUSEL', { data: results.data, fetchDate: today });
            }
          },
    },
    plugins: [new VuexPersistence().plugin],
});
