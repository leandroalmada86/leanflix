import Vue from 'vue'
import App from './App.vue'
import VueSession from 'vue-session'
Vue.use(VueSession)
import router from './router'
import Vuex from 'vuex'
import store from './store'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
// import VeeValidate from 'vee-validate';
// Vue.use(VeeValidate);
Vue.use(Vuex)

Vue.config.productionTip = false

new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App),
}).$mount('#app')
